import React from 'react';
import CardFilm from '../CardFilm';
import {getTop} from '../../services/kinopoiskAPI';

import s from './FilmsList.module.scss';

class FilmsList extends React.Component {
    state = {
        films: []
    };

    createFilmsList = async () => {
        let filmsList = [];
        let FILMS = await getTop();
        
        filmsList = FILMS.films;

        this.setState({
            films: filmsList
        })
    }


    componentDidMount() {
        this.createFilmsList();
    }

    render() {
        const films = this.state.films;
        return (
            <>
            <h1 className={s.title}>Золотые хиты</h1>
            <div className={s.filmsList}>
                {films.map((el) => <CardFilm key={el.filmId} id={el.filmId} rating={el.rating} name={el.nameRu} genres={el.genres} descr={el.year} poster={el.posterUrl} /> )}
            </div>
            </>
        );
    }
}

export default FilmsList;
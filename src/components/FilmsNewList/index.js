import React, { useState, useEffect } from 'react';
import { getReleasesNow } from '../../services/kinopoiskAPI';
import CardFilm from '../CardFilm';

import s from './FilmsNewList.module.scss';

const FilmsNewList = () => {
    const [newFilms, setNewFilms] = useState([]);

    useEffect(() => {
        const createFilmsList = async () => {
            let filmsList = [];
            let FILMS = await getReleasesNow();
            filmsList = FILMS.releases;  
            setNewFilms(filmsList)
        }
        createFilmsList();
    }, []);

    return (
        <>
            <h1 className={s.title}>Цифровые релизы месяца</h1>
            <div className={s.filmsList}>
                {newFilms.map((el) => <CardFilm key={el.filmId} id={el.filmId} rating={el.rating.toFixed(1)} name={el.nameRu} genres={el.genres} descr={el.year} poster={el.posterUrl} /> )}
            </div>
        </>
    );
};

export default FilmsNewList;
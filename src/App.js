import React, { useState } from 'react';
import {Route, NavLink, Switch} from 'react-router-dom';
import FilmsList from './components/FilmsList';
import { Menu, Layout } from 'antd';
import 'antd/dist/antd.css';
import SearchList from './components/SearchList';
import FilmInfo from './components/FilmInfo';
import FilmsNewList from './components/FilmsNewList';

function App() {
  const [current, setCurrent] = useState('home');
  const {Footer} = Layout;

  return (
      <React.Fragment>
        <div className="main">
        <Menu onClick={(e) => setCurrent(e.key)} selectedKeys={[current]} mode="horizontal">
          <Menu.Item key="home">
            <NavLink to="/" exact>Главная</NavLink>
          </Menu.Item>
          <Menu.Item key="new-films">            
            <NavLink to="/new-films">Новинки</NavLink>
          </Menu.Item>
          <Menu.Item key="search">            
            <NavLink to="/search">Поиск фильмов</NavLink>
          </Menu.Item>
        </Menu>

      <Switch>
        <Route path="/" exact component={FilmsList} />
        <Route path="/search" component={SearchList} />
        <Route path="/new-films" component={FilmsNewList} />
        <Route path="/films/:id" render={() => <FilmInfo />} />
        <Route render={() => <h1>404 Error</h1>}/>
      </Switch>
        
        </div>
        
        <Footer className='footer' style={{ textAlign: 'center' }}>Created by blikov_d</Footer>
      </React.Fragment>
  );
}
//Лучшие фильмы
//Фильмы этого месяца*
//Поиск по ключевым словам
//Отдельные страницы с фильмом*

export default App;

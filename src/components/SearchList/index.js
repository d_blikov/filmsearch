import React from 'react';

import {searchByKeywords} from '../../services/kinopoiskAPI';
import {Input} from 'antd';
import s from './SearchList.module.scss'

import CardFilm from '../CardFilm'

const {Search} = Input;

class SearchList extends React.Component {

    state = {
        searchList: [],
        searchFilm: ''
    };

    searchFilm = async (keyword) => {
        const res = await searchByKeywords(keyword);
        return res;
    }

    changeSearch = async (val) => {
        let res = await this.searchFilm(val);

        this.setState({
            searchFilm: val,
            searchList: await res.films
        })
    }

    render() {        
        return (
          <div className={s.SearchList}>
            <div className={s.searchPanel}>
            <Search
              placeholder="Search your favorite film"
              onSearch={this.changeSearch}
              enterButton
            />
            </div>

            <div className={s.filmsList}>
                {this.state.searchList.map(el => <CardFilm key={el.filmId} id={el.filmId} rating={el.rating} name={el.nameRu} genres={el.genres} descr={el.year} poster={el.posterUrl} /> )}
            </div>
          </div>
        );
    }
};

export default SearchList;
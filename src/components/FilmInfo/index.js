import React, { useState, useEffect } from "react";
import { Rate } from "antd";
import { useParams } from "react-router-dom";
import s from "./filmInfo.module.scss";

import { getFilmInfo, getTrailer } from "../../services/kinopoiskAPI";

const FilmInfo = () => {
  const { id } = useParams();

  const getFilmById = async (id) => {
    const filmInfo = await getFilmInfo(id);
    return filmInfo.data;
  };

  const getRateById = async (id) => {
    const filmRate = await getFilmInfo(id);
    return filmRate.rating.rating;
  };

  const getTrailerById = async (id) => {
    const trailer = await getTrailer(id);
    if (trailer.trailers[0]) {
      console.log("trailer : ", trailer);
      let firstStr = trailer.trailers[0].url;
      let idx = firstStr.indexOf("=");
      let hash = firstStr.slice(idx + 1);
      let newStr = "https://www.youtube.com/embed/" + hash;
      return newStr;
    }
    return false;
  };

  const [film, setFilm] = useState({});
  const [rate, setRate] = useState("");
  const [trailer, setTrailer] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const film = await getFilmById(id);
      const rate = await getRateById(id);
      const trailer = await getTrailerById(id);
      setFilm(film);
      setRate(rate);
      setTrailer(trailer);
    };

    fetchData();
  }, [id]);

  console.log(film);
  return (
    <div className={s.film}>
      <div className={s.filmMain}>
        <div className={s.filmPosterWrap}>
          <img
            className={s.filmPoster}
            src={film.posterUrl}
            alt={film.posterUrl}
          ></img>
          <div className={s.rateWrap}>
            <Rate
              disabled
              defaultValue={0}
              value={rate}
              allowHalf
              count={10}
              style={{ fontSize: 13 }}
            />
            <div>{rate}</div>
          </div>
        </div>

        <div className={s.filmInfo}>
          <div className={s.filmInfoInner}>
            <h3>{film.nameRu}</h3>
            <p>{film.nameEn}</p>
            <h3>О фильме</h3>
            <p>Год: {film.year}</p>
            <p>Описание: {film.description}</p>
            <p>Возрастной рейтинг: {film.ratingAgeLimits}+</p>
            <p>Премьера (мир): {film.premiereWorld}</p>
            <p>Премьера (РФ): {film.premiereRu}</p>
            <p>Длительность: {film.filmLength}</p>
          </div>
        </div>
      </div>

      {trailer ? (
        <div className={s.frameWrap}>
          <iframe
            title={film.nameRu}
            width="560"
            height="315"
            src={trailer}
            frameBorder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>
        </div>
      ) : null}
    </div>
  );
};

export default FilmInfo;

import React from 'react';
import {withRouter} from 'react-router-dom';
import { Card } from 'antd';

import s from './CardFilm.module.scss';


const cardFilm = (props) => {
    const { Meta } = Card;
    
    const genres = props.genres;
    let genresStr = genres.reduce((acc, val, idx) => {
        if (idx === 0) {
            return acc + val.genre
        }
        return acc + ', ' + val.genre
    }, '');

    const openFilmInfo = () => {
        const id = props.id;
        props.history.push(`/films/${id}`)
    }

    return (
        <div className={s.card} onClick={openFilmInfo}>
            
            <Card
                hoverable
                style={{ width: 220 }}
                cover={<img src={props.poster} alt={props.id}/>}
            >
                <p className={s.rate}>{props.rating}</p>
                <Meta title={props.name} description={genresStr}/>
            </Card>

        </div>
    )
}

export default withRouter(cardFilm);
const key = process.env.REACT_APP_X_API_KEY;

export const getFilmInfo = async (filmId) => {
    const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.1/films/${filmId}?append_to_response=RATING`, {
        method: 'GET',
        headers: {
            'X-API-KEY': key
        }
    })
    let body = await res.json();

    return await body;
}

export const getTop = async () => {
    const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.1/films/top`, {
        method: 'GET',
        headers: {
            'X-API-KEY': key
        }
    })
    let body = await res.json();

    return await body;
}

export const searchByKeywords = async (keyword) => {
    const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword=${keyword}`, {
        method: 'GET',
        headers: {
            'X-API-KEY': key
        }
    })
    let body = await res.json();

    return await body;
};

export const getTrailer = async (filmId) => {
  const res = await fetch(`https://kinopoiskapiunofficial.tech/api/v2.1/films/${filmId}/videos`, {
      method: 'GET',
      headers: {
          'X-API-KEY': key
      }
  })
  let body = await res.json();

  return await body;
}


export const getReleasesNow = async () => {
  const getMonth = (month) => {
    const monthsList = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    return monthsList[month];
  };

  let now = new Date();
  let year = now.getFullYear();
  let month = getMonth(now.getMonth());
  console.log("###month: ", month);

  const res = await fetch(
    `https://kinopoiskapiunofficial.tech/api/v2.1/films/releases?year=${year}&month=${month}&page=1`,
    {
      method: "GET",
      headers: {
        "X-API-KEY": key,
      },
    }
  );
  let body = await res.json();
  return await body;
};

/* (async() => {
    const res = await getInfo(301)
    console.log(res.posterUrl)
    console.log(res.nameRu)
})() */
